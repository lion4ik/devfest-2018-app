package com.redmadrobot.devfestapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main_demo.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_demo)
    }

    override fun onBackPressed() {
        if (motionLayout.currentState == R.id.end) {
            motionLayout.transitionToStart()
        } else {
            super.onBackPressed()
        }
    }
}
